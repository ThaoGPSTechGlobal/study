﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CreateTheFormLogin.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}